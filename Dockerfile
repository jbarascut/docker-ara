FROM centos/httpd
MAINTAINER Jeremy Barascut 
#ENV ARA_DATABASE="postgresql+psycopg2://ara:password@localhost:5432/ara"

USER root

RUN yum -y update && yum -y install epel-release gcc python-devel which mod_wsgi && yum -y install python-pip && yum -y clean all
RUN pip install psycopg2 ara>=0.16
RUN mkdir -p /var/www/ara{,/.ansible/tmp,.ara}

RUN cp -p $(which ara-wsgi) /var/www/ara/

COPY ansible.cfg /var/www/ara/ansible.cfg
COPY ara.conf /etc/httpd/conf.d/ara.conf
RUN chown -R apache:apache /var/www/ara
EXPOSE 80
CMD ["./run-httpd.sh"]
